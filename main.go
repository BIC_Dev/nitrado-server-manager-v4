package main

import (
	"context"

	"github.com/caarlos0/env/v6"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/api/controllers"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/api/routes"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Environment struct
type Environment struct {
	Environment              string `env:"ENVIRONMENT,required"`
	DiscordToken             string `env:"DISCORD_TOKEN,required"`
	ServiceToken             string `env:"SERVICE_TOKEN,required"`
	NitradoServiceV3Host     string `env:"NITRADO_SERVICE_V3_HOST,required"`
	NitradoServiceV3BasePath string `env:"NITRADO_SERVICE_V3_BASE_PATH,required"`
	NitradoServiceV3Token    string `env:"NITRADO_SERVICE_V3_TOKEN,required"`
	GuildConfigServiceToken  string `env:"GUILD_CONFIG_SERVICE_TOKEN,required"`
}

func main() {
	ctx := context.Background()

	environment := Environment{}
	if err := env.Parse(&environment); err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to load environment variables."))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("env", environment.Environment),
	)

	config, cErr := configs.GetConfig(ctx, environment.Environment)
	if cErr != nil {
		eCtx := logging.AddValues(cErr.Ctx, zap.NamedError("error", cErr.Err), zap.String("error_message", cErr.Msg))
		logger := logging.Logger(eCtx)
		logger.Fatal("error_log")
	}

	cacheClient, cacheErr := cache.GetClient(ctx, config.Redis.Host, config.Redis.Port, config.Redis.Pool)
	if cacheErr != nil {
		eCtx := logging.AddValues(ctx, zap.NamedError("error", cacheErr.Err), zap.String("error_message", cacheErr.Message))
		logger := logging.Logger(eCtx)
		logger.Fatal("error_log")
	}

	gc := guildconfigservice.InitService(ctx, config, environment.GuildConfigServiceToken)

	sess, dsErr := bot.OpenDiscordSession(ctx, environment.DiscordToken)
	if dsErr != nil {
		eCtx := logging.AddValues(dsErr.Ctx, zap.NamedError("error", dsErr.Err), zap.String("error_message", dsErr.Msg))
		logger := logging.Logger(eCtx)
		logger.Fatal("error_log")
	}

	defer sess.Close()

	bot := bot.Bot{
		Config:            config,
		Environment:       environment.Environment,
		Session:           sess,
		NSV3Client:        nsv3.CreateClientWrapper(environment.NitradoServiceV3Host, environment.NitradoServiceV3BasePath, environment.NitradoServiceV3Token),
		GuildConfigClient: gc,
		Cache:             cacheClient,
	}

	bErr := bot.StartBot(ctx)
	if bErr != nil {
		eCtx := logging.AddValues(bErr.Ctx, zap.NamedError("error", bErr.Err), zap.String("error_message", bErr.Msg))
		logger := logging.Logger(eCtx)
		logger.Fatal("error_log")
	}

	controller := controllers.Controller{
		Config: config,
		Cache:  cacheClient,
	}

	routerStruct := routes.Router{
		Controller:   &controller,
		ServiceToken: environment.ServiceToken,
		Port:         config.API.Port,
		BasePath:     config.API.BasePath,
	}

	router := routes.GetRouter(ctx)
	routes.AddRoutes(ctx, router, &routerStruct)

	// Temporary to keep main from closing
	// wg := &sync.WaitGroup{}
	// wg.Add(1)
	// wg.Wait()
}
