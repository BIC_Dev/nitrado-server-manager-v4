REDIS:
  host: "host.docker.internal"
  port: 6379
  pool: 10
NITRADO_SERVICE_V3:
  timeout: 30
GUILD_CONFIG_SERVICE:
  host: "host.docker.internal:8082"
  base_path: "/guild-config-service"
API:
  port: 8080
  base_path: /nitrado-server-manager-v4
BOT:
  prefix: "n!"
  ok_color: 0x3AB795
  warn_color: 0xFFD166
  error_color: 0xEE6352
  working_thumbnail: https://cdn.discordapp.com/emojis/833494790326714379.png?v=1
  ok_thumbnail: https://cdn.discordapp.com/emojis/835676837320720406.png?v=1
  warn_thumbnail: https://cdn.discordapp.com/emojis/835417781960835074.png?v=1
  error_thumbnail: https://cdn.discordapp.com/emojis/835417781913649152.png?v=1
  documentation_url: "https://gitlab.com/BIC_Dev/nitrado-server-manager-v3/-/wikis/Home"
  guild_service: "nitrado-server-manager-v4"
CACHE_SETTINGS:
  activation_token:
    base: "ACTIVATION_TOKEN"
    ttl: "604800" # 7 days
    enabled: true
  setup_nitrado_token:
    base: "SETUP_NITRADO_TOKEN"
    ttl: "3600" # 1 hour
    enabled: true
COMMANDS:
  - name: "help"
    title: "Help Information"
    description: "Get information about the bot and its commands."
    enabled: true
    options:
      - name: "all"
        description: "Get information about all commands in the bot."
        enabled: true
        required: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "player"
        description: "Handles banning / unbanning players and searching for players by account name."
        enabled: true
        required: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "server"
        enabled: true
        description: "Handles stopping / restarting and listing servers."
        required: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "setup"
        enabled: true
        description: "Handles setting up the bot."
        required: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "info"
        enabled: true
        description: "Handles getting info about the bot and your setup."
        required: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
    type: 1
  - name: "player"
    title: "Player Controls"
    description: "Handles banning / unbanning players and searching for players by account name."
    enabled: true
    options:
      - name: "ban"
        title: "Ban Player"
        description: "Ban a player on one or multiple servers in your cluster by account name."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          - name: "account"
            description: "Account name of the player you want to ban."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "server_id"
            description: "Server ID where you want to ban the player. Don't set if you want to ban across all servers."
            enabled: true
            required: false
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "reason"
            description: "Reason for banning the player."
            enabled: true
            required: false
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "unban"
        title: "Unban Player"
        description: "Unban a player on one or multiple servers in your cluster by account name."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          - name: "account"
            description: "Account name of the player you want to unban."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "server_id"
            description: "Server ID where you want to unban the player. Don't set if you want to unban across all servers."
            enabled: true
            required: false
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "reason"
            description: "Reason for unbanning the player."
            enabled: true
            required: false
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "search"
        title: "Search Players"
        description: "Search for a player across your cluster by account name."
        enabled: false
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          -
            name: "account"
            description: "Account name of the player you want to search for."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
  - name: "server"
    title: "Server Controls"
    description: "Handles managing your linked servers."
    enabled: true
    options:
      - name: "stop"
        title: "Stop Server"
        description: "Execute a force-stop on your server."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          - name: "server_id"
            description: "Identifier of server you want to stop."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "message"
            description: "Message to display in-game when stopping the server. This is unreliable."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "restart"
        title: "Restart Server"
        description: "Execute a restart on your server. If the server is currently stopped, this will start the server."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          - name: "server_id"
            description: "Identifier of server you want to restart."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "message"
            description: "Message to display in-game when restarting the server. This is unreliable."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "rename"
        title: "Rename Server"
        description: "Rename your server. This will not effect your public server name."
        enabled: true
        required: false
        send_deferred_message: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          -
            name: "server_id"
            description: "Identifier of server you want to stop."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          -
            name: "name"
            description: "New name for the server."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "remove"
        title: "Remove Server"
        description: "Remove a server from the bot."
        enabled: true
        required: false
        send_deferred_message: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          -
            name: "server_id"
            description: "Identifier of server you want to stop."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
  - name: "setup"
    title: "Setup Bot"
    description: "Handles setting up the bot."
    enabled: true
    dm_permission: true
    options:
      - name: "activate"
        title: "Activate Bot"
        description: "Use a provided activation token to activate the bot on your Discord."
        enabled: true
        required: false
        send_deferred_message: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
        options:
          - name: "token"
            description: "Activation token provided by BIC Development support team."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
          - name: "discord_name"
            description: "Name for your discord server in the bot."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 0
      - name: "servers"
        title: "Link Servers"
        description: "Automatically retreive and link your servers to the bot."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "nitrado_token"
        title: "Nitrado Token DM"
        description: "Opens a DM with the bot to link your Nitrado tokens."
        enabled: true
        required: false
        send_deferred_message: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
      - name: "add_token"
        title: "Add Nitrado Token"
        description: "Adds a Nitrado long-life token to the bot for your Discord."
        enabled: true
        required: false
        send_deferred_message: true
        type: 1
        auto_complete: true
        channel_types:
          - 1
        options:
          - name: "token"
            description: "Nitrado Long-life token."
            enabled: true
            required: true
            type: 3
            auto_complete: false
            channel_types:
              - 1
  - name: "info"
    title: "Bot Information"
    description: "Handles getting info about the bot and your setup."
    enabled: true
    dm_permission: false
    options:
      - name: "servers"
        title: "Linked Servers"
        description: "List all your servers and detailed information about them."
        enabled: true
        required: false
        send_deferred_message: false
        type: 1
        auto_complete: true
        channel_types:
          - 0
