package configs

import (
	"context"
	"os"
	"time"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	Redis struct {
		Host string `yaml:"host"`
		Port int    `yaml:"port"`
		Pool int    `yaml:"pool"`
	} `yaml:"REDIS"`
	NitradoServiceV3 struct {
		Timeout time.Duration `yaml:"timeout"`
	} `yaml:"NITRADO_SERVICE"`
	GuildConfigService struct {
		Host     string `yaml:"host"`
		BasePath string `yaml:"base_path"`
	} `yaml:"GUILD_CONFIG_SERVICE"`
	CacheSettings struct {
		ActivationToken   CacheSetting `yaml:"activation_token"`
		SetupNitradoToken CacheSetting `yaml:"setup_nitrado_token"`
	} `yaml:"CACHE_SETTINGS"`
	Bot struct {
		OkColor          int    `yaml:"ok_color"`
		WarnColor        int    `yaml:"warn_color"`
		ErrorColor       int    `yaml:"error_color"`
		DocumentationURL string `yaml:"documentation_url"`
		GuildService     string `yaml:"guild_service"`
		WorkingThumbnail string `yaml:"working_thumbnail"`
		OkThumbnail      string `yaml:"ok_thumbnail"`
		WarnThumbnail    string `yaml:"warn_thumbnail"`
		ErrorThumbnail   string `yaml:"error_thumbnail"`
	} `yaml:"BOT"`
	API struct {
		Port     string `yaml:"port"`
		BasePath string `yaml:"base_path"`
	} `yaml:"API"`
	Commands []Command `yaml:"COMMANDS"`
}

// CacheSetting struct
type CacheSetting struct {
	Base    string `yaml:"base"`
	TTL     string `yaml:"ttl"`
	Enabled bool   `yaml:"enabled"`
}

// Command struct
type Command struct {
	Name                string          `yaml:"name"`
	Title               string          `yaml:"title"`
	Description         string          `yaml:"description"`
	Options             []CommandOption `yaml:"options"`
	Type                int             `yaml:"type"`
	Enabled             bool            `yaml:"enabled"`
	DMPermission        bool            `yaml:"dm_permission"`
	SendDeferredMessage bool            `yaml:"send_deferred_message"`
}

// CommandOptions struct
type CommandOption struct {
	Name        string `json:"name"`
	Title       string `yaml:"title"`
	Description string `json:"description,omitempty"`
	Required    bool   `json:"required"`
	Type        int    `yaml:"type"`

	// NOTE: mutually exclusive with Choices.
	Autocomplete bool `json:"auto_complete"`

	ChannelTypes        []int                 `yaml:"channel_types"`
	Options             []CommandOption       `yaml:"options"`
	Choices             []CommandOptionChoice `yaml:"choices"`
	Enabled             bool                  `yaml:"enabled"`
	SendDeferredMessage bool                  `yaml:"send_deferred_message"`
}

// CommandOptionChoice struct
type CommandOptionChoice struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
}

type Error struct {
	Msg string
	Err error
	Ctx context.Context
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func (e *Error) Context() context.Context {
	return e.Ctx
}

func (e *Error) Message() string {
	return e.Msg
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(ctx context.Context, env string) (*Config, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		return nil, &Error{
			Msg: "Failed to open config file",
			Err: err,
			Ctx: ctx,
		}
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		return nil, &Error{
			Msg: "Failed to decode config file",
			Err: err,
			Ctx: ctx,
		}
	}

	return &config, nil
}
