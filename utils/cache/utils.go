package cache

import (
	"fmt"
)

// GenerateKey func
func GenerateKey(base string, id interface{}) string {
	return fmt.Sprintf("%s:%s", base, fmt.Sprint(id))
}
