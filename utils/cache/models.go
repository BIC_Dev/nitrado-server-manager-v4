package cache

type Player struct {
	Name   string `json:"name"`
	ID     string `json:"id"`
	IDType string `json:"id_type"`
	Online string `json:"online"`
}

type ServerPlayers struct {
	Players []Player `json:"players"`
}

type ActivationToken struct {
	Token string `json:"token"`
}

type SetupNitradoToken struct {
	UserID  string `json:"user_id"`
	GuildID string `json:"guild_id"`
}
