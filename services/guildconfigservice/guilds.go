package guildconfigservice

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guilds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
)

// GetAllGuilds func
func GetAllGuilds(ctx context.Context, gcs *GuildConfigService) (*guilds.GetAllGuildsOK, *Error) {
	guildParams := guilds.NewGetAllGuildsParamsWithTimeout(30 * time.Second)
	guildParams.Context = context.Background()

	guild, gfErr := gcs.Client.Guilds.GetAllGuilds(guildParams, gcs.Auth)
	if gfErr != nil {
		return guild, &Error{
			Message: "Failed to get all guilds",
			Err:     gfErr,
		}
	}

	return guild, nil
}

// CreateGuild func
func CreateGuild(ctx context.Context, gcs *GuildConfigService, guildID string, guildName string) (*guilds.CreateGuildCreated, *Error) {
	params := guilds.NewCreateGuildParamsWithTimeout(30 * time.Second)
	params.SetContext(context.Background())
	params.SetBody(&gcscmodels.Guild{
		Enabled: true,
		ID:      guildID,
		Name:    guildName,
	})

	guild, gErr := gcs.Client.Guilds.CreateGuild(params, gcs.Auth)
	if gErr != nil {
		return guild, &Error{
			Message: "Failed to create guild",
			Err:     gErr,
		}
	}

	return guild, nil
}
