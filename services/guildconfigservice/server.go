package guildconfigservice

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/servers"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
)

func RenameServer(ctx context.Context, gcs *GuildConfigService, server *gcscmodels.Server, name string) (*servers.UpdateServerOK, *Error) {
	params := servers.NewUpdateServerParamsWithTimeout(30 * time.Second)
	params.SetContext(context.Background())
	params.SetGuild(server.GuildID)
	params.SetServerID(int64(server.ID))
	params.SetBody(&gcscmodels.UpdateServerRequest{
		BoostSettingID: server.BoostSettingID,
		Enabled:        server.Enabled,
		GuildID:        server.GuildID,
		Name:           name,
		NitradoID:      server.NitradoID,
		NitradoTokenID: server.NitradoTokenID,
		ServerTypeID:   server.ServerTypeID,
	})

	resp, err := gcs.Client.Servers.UpdateServer(params, gcs.Auth)
	if err != nil {
		return nil, &Error{
			Message: "Failed to rename server",
			Err:     err,
		}
	}

	return resp, nil
}

func DeleteServer(ctx context.Context, gcs *GuildConfigService, server *gcscmodels.Server) (*servers.DeleteServerOK, *Error) {
	params := servers.NewDeleteServerParamsWithTimeout(30 * time.Second)
	params.SetContext(context.Background())
	params.SetGuild(server.GuildID)
	params.SetServerID(int64(server.ID))

	resp, err := gcs.Client.Servers.DeleteServer(params, gcs.Auth)
	if err != nil {
		return nil, &Error{
			Message: "Failed to delete server",
			Err:     err,
		}
	}

	return resp, nil
}
