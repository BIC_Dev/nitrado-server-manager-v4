package guildconfigservice

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_services"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
)

// CreateGuild func
func CreateGuildService(ctx context.Context, gcs *GuildConfigService, guildID string, guildService string) (*guild_services.CreateGuildServiceCreated, *Error) {
	params := guild_services.NewCreateGuildServiceParamsWithTimeout(30 * time.Second)
	params.SetContext(context.Background())
	params.SetGuild(guildID)
	params.SetBody(&gcscmodels.GuildService{
		Enabled: true,
		GuildID: guildID,
		Name:    guildService,
	})

	guild, gErr := gcs.Client.GuildServices.CreateGuildService(params, gcs.Auth)
	if gErr != nil {
		return guild, &Error{
			Message: "Failed to create guild service",
			Err:     gErr,
		}
	}

	return guild, nil
}

// ActivateGuildService func
func ActivateGuildService(ctx context.Context, gcs *GuildConfigService, guildService *gcscmodels.GuildService, guildName string) (*guild_services.UpdateGuildServiceOK, *Error) {
	params := guild_services.NewUpdateGuildServiceParamsWithTimeout(30 * time.Second)
	params.SetContext(context.Background())
	params.SetGuildServiceID(int64(guildService.ID))
	params.SetGuild(guildService.GuildID)
	params.SetBody(&gcscmodels.UpdateGuildServiceRequest{
		Enabled: true,
		GuildID: guildService.GuildID,
		Name:    guildName,
	})

	gs, gsErr := gcs.Client.GuildServices.UpdateGuildService(params, gcs.Auth)
	if gsErr != nil {
		return gs, &Error{
			Message: "Failed to activate guild service",
			Err:     gsErr,
		}
	}

	return gs, nil
}
