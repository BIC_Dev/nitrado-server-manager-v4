package guildconfigservice

import (
	"context"
	"time"

	"github.com/google/uuid"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/nitrado_tokens"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
)

func AddNitradoToken(ctx context.Context, gcs *GuildConfigService, guildID string, token string) (*nitrado_tokens.CreateNitradoTokenCreated, *Error) {
	params := nitrado_tokens.NewCreateNitradoTokenParamsWithTimeout(30 * time.Second)
	params.SetGuild(guildID)
	params.SetBody(&gcscmodels.NitradoToken{
		Token:      uuid.New().String(),
		TokenValue: token,
		GuildID:    guildID,
		Enabled:    true,
	})
	params.SetContext(context.Background())

	resp, err := gcs.Client.NitradoTokens.CreateNitradoToken(params, gcs.Auth)
	if err != nil {
		return nil, &Error{
			Message: "Failed to add Nitrado token",
			Err:     err,
		}
	}

	return resp, nil
}
