package nsv3

import (
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/strfmt"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient"
)

// Nitrado Service V3 Client Wrapper
type NSV3ClientWrapper struct {
	Client *nsclient.Nscv3
	Auth   runtime.ClientAuthInfoWriter
}

func CreateClientWrapper(host string, basepath string, token string) *NSV3ClientWrapper {
	// Create Nitrado Service V3 Client
	nsctransport := nsclient.TransportConfig{
		Host:     host,
		BasePath: basepath,
		Schemes:  nil,
	}
	return &NSV3ClientWrapper{
		Client: nsclient.NewHTTPClientWithConfig(strfmt.Default, &nsctransport),
		Auth:   NitradoServiceV3Token(token),
	}
}

// NitradoServiceV3Token func
func NitradoServiceV3Token(token string) runtime.ClientAuthInfoWriter {
	return runtime.ClientAuthInfoWriterFunc(func(r runtime.ClientRequest, _ strfmt.Registry) error {
		return r.SetHeaderParam("Service-Token", token)
	})
}
