package discord

import "github.com/bwmarrin/discordgo"

// CreateDMChannel func
func CreateDMChannel(session *discordgo.Session, userID string) (*discordgo.Channel, *Error) {
	dmChannel, dmErr := session.UserChannelCreate(userID)
	if dmErr != nil {
		return nil, ParseDiscordError(dmErr)
	}

	return dmChannel, nil
}
