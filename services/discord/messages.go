package discord

import "github.com/bwmarrin/discordgo"

func SendMessageEmbed(session *discordgo.Session, channelID string, embeds []*discordgo.MessageEmbed) (*discordgo.Message, *Error) {
	message, err := session.ChannelMessageSendEmbeds(channelID, embeds)
	if err != nil {
		return nil, ParseDiscordError(err)
	}

	return message, nil
}
