package discord

import (
	"context"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

var MAX_EMBED_CHARACTERS = 5000 // 6000
var MAX_FIELD_CHARACTERS = 800  // 1000
var MAX_FIELDS = 20             // 25?

type Response struct {
	Type        discordgo.EmbedType
	Title       string
	Description string
	URL         string
	Color       int
	Timestamp   string
	Footer      Footer
	Thumbnail   Thumbnail
	Author      Author
	Fields      []Field
}

type Footer struct {
	IconURL string
	Text    string
}

type Thumbnail struct {
	URL    string
	Width  int
	Height int
}

type Author struct {
	Name    string
	URL     string
	IconURL string
}

type Field struct {
	Name   string
	Value  string
	Inline bool
}

func (r *Response) GenerateEmbeds(ctx context.Context) ([]*discordgo.MessageEmbed, error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var embeds []*discordgo.MessageEmbed

	embedTemplate := generateEmbedTemplate(r)

	currentEmbed := embedTemplate
	currentEmbed.URL = r.URL

	currentEmbedCharacters := 0
	currentEmbedFieldCount := 0
	currentFieldCount := 0
	totalFields := len(r.Fields)

	if totalFields == 0 {
		embeds = append(embeds, &currentEmbed)
		return embeds, nil
	}

	for _, field := range r.Fields {
		fieldChars := calcFieldChars(field)
		if fieldChars+currentEmbedCharacters > MAX_EMBED_CHARACTERS {
			var tempEmbed discordgo.MessageEmbed = currentEmbed
			embeds = append(embeds, &tempEmbed)
			currentEmbed = embedTemplate
			currentEmbedCharacters = 0
			currentEmbedFieldCount = 0
		}

		if currentEmbedFieldCount >= MAX_FIELDS {
			var tempEmbed discordgo.MessageEmbed = currentEmbed
			embeds = append(embeds, &tempEmbed)
			currentEmbed = embedTemplate
			currentEmbedCharacters = 0
			currentEmbedFieldCount = 0
		}

		currentEmbed.Fields = append(currentEmbed.Fields, &discordgo.MessageEmbedField{
			Name:   field.Name,
			Value:  field.Value,
			Inline: field.Inline,
		})

		currentEmbedCharacters += fieldChars
		currentEmbedFieldCount += 1
		currentFieldCount += 1

		if currentFieldCount == totalFields {
			embeds = append(embeds, &currentEmbed)
			break
		}
	}

	return embeds, nil
}

func generateEmbedTemplate(r *Response) discordgo.MessageEmbed {
	var embedTemplate discordgo.MessageEmbed

	if strings.Trim(r.Title, " ") != "" {
		embedTemplate.Title = r.Title
	}

	if strings.Trim(r.Description, " ") != "" {
		embedTemplate.Description = r.Description
	}

	embedTemplate.Color = r.Color

	embedTemplate.Timestamp = r.Timestamp

	embedTemplate.Footer = &discordgo.MessageEmbedFooter{
		Text:    r.Footer.Text,
		IconURL: r.Footer.IconURL,
	}

	embedTemplate.Thumbnail = &discordgo.MessageEmbedThumbnail{
		URL:    r.Thumbnail.URL,
		Width:  r.Thumbnail.Width,
		Height: r.Thumbnail.Height,
	}

	if strings.Trim(r.Author.Name, " ") != "" {
		embedTemplate.Author = &discordgo.MessageEmbedAuthor{
			Name:    r.Author.Name,
			IconURL: r.Author.IconURL,
			URL:     r.Author.URL,
		}
	}

	return embedTemplate
}

func calcFieldChars(field Field) int {
	return len(field.Name) + len(field.Value)
}

func InteractionResponse(session *discordgo.Session, interaction *discordgo.Interaction, response *discordgo.InteractionResponse) error {
	return session.InteractionRespond(interaction, response)
}

func InteractionEdit(session *discordgo.Session, interaction *discordgo.Interaction, edit *discordgo.WebhookEdit) (*discordgo.Message, error) {
	return session.InteractionResponseEdit(interaction, edit)
}
