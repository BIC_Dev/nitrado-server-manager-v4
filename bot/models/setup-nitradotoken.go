package models

import (
	"context"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type SetupNitradoToken struct {
	Config            *configs.Config
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	Cache             *cache.Cache
	GuildConfigClient *guildconfigservice.GuildConfigService
	GuildID           string
	User              *discordgo.User
	Session           *discordgo.Session
}

func (model *SetupNitradoToken) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: "Failed to find linked Discord server",
			Err: fErr,
			Ctx: ctx,
		}
	}

	gvErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "GuildServices")
	if gvErr != nil {
		return nil, &Error{
			Msg: "Bot is not activated for this Discord server",
			Err: gvErr,
			Ctx: ctx,
		}
	}

	dmChannel, dmErr := discord.CreateDMChannel(model.Session, model.User.ID)
	if dmErr != nil {
		return nil, &Error{
			Msg: dmErr.Message,
			Err: dmErr.Err,
			Ctx: ctx,
		}
	}

	dmResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**/%s %s**", model.Command.Name, model.CommandOption.Name),
		Description: "Please run the \"/setup add_token\" command IN THIS DM to add a new Nitrado long-life token to the Nitrado Server Manager V4.",
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
	}

	dmEmbeds, dmEmbedsErr := dmResponse.GenerateEmbeds(ctx)
	if dmEmbedsErr != nil {
		return nil, &Error{
			Msg: "Failed to generate embeds.",
			Err: dmEmbedsErr,
			Ctx: ctx,
		}
	}

	_, dmMsgErr := discord.SendMessageEmbed(model.Session, dmChannel.ID, dmEmbeds)
	if dmMsgErr != nil {
		return nil, &Error{
			Msg: dmMsgErr.Message,
			Err: dmMsgErr.Err,
			Ctx: ctx,
		}
	}

	var setupNitradoToken *cache.SetupNitradoToken = &cache.SetupNitradoToken{
		UserID:  model.User.ID,
		GuildID: model.GuildID,
	}
	cacheKey := cache.GenerateKey(model.Config.CacheSettings.SetupNitradoToken.Base, model.User.ID)
	cErr := model.Cache.SetStruct(ctx, cacheKey, &setupNitradoToken, model.Config.CacheSettings.SetupNitradoToken.TTL)
	if cErr != nil {
		return nil, &Error{
			Msg: "Unable to cache setup nitrado token details",
			Err: cErr,
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	fields := generateSetupNitradoTokenFields()

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Setup Nitrado Token command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateSetupNitradoTokenFields() []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   "**Check your DMs from the Nitrado Server Manager V4**",
		Value:  "You will add your Nitrado Token by replying to the DM from the bot. DO NOT SUBMIT YOUR NITRADO TOKEN HERE!",
		Inline: false,
	})

	return fields
}
