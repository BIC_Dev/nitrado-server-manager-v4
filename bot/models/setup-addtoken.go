package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient/tokens"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"
	"go.uber.org/zap"
)

type SetupAddToken struct {
	Config            *configs.Config
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	Cache             *cache.Cache
	GuildConfigClient *guildconfigservice.GuildConfigService
	NSV3Client        *nsv3.NSV3ClientWrapper
	User              *discordgo.User
	NitradoToken      string
}

func (model *SetupAddToken) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var setupNitradoToken *cache.SetupNitradoToken
	cacheKey := cache.GenerateKey(model.Config.CacheSettings.SetupNitradoToken.Base, model.User.ID)
	cErr := model.Cache.GetStruct(ctx, cacheKey, &setupNitradoToken)
	if cErr != nil {
		return nil, &Error{
			Msg: "Failed to retrieve cached setup nitrado token details",
			Err: cErr.Err,
			Ctx: ctx,
		}
	}

	if setupNitradoToken == nil {
		return nil, &Error{
			Msg: "Cannot identify guild to add Nitrado long-life token.",
			Err: errors.New("no guild found in cache"),
			Ctx: ctx,
		}
	}

	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, setupNitradoToken.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: "Failed to find linked Discord server",
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	gvErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "GuildServices")
	if gvErr != nil {
		return nil, &Error{
			Msg: "Bot is not activated for this Discord server",
			Err: gvErr.Err,
			Ctx: ctx,
		}
	}

	var existingTokens []*nsmodels.Token
	if feed.Payload.Guild.NitradoTokens != nil {
		for _, token := range feed.Payload.Guild.NitradoTokens {
			tokenID := token.Token
			tokenValue := token.TokenValue
			existingTokens = append(existingTokens, &nsmodels.Token{
				ID:    &tokenID,
				Value: &tokenValue,
			})
		}
	}

	validateParams := tokens.NewValidateTokenParamsWithTimeout(model.Config.NitradoServiceV3.Timeout * time.Second)
	validateParams.SetBody(tokens.ValidateTokenBody{
		ExistingTokens: existingTokens,
		NewToken:       &model.NitradoToken,
	})
	validateParams.SetContext(ctx)

	validateResp, vErr := model.NSV3Client.Client.Tokens.ValidateToken(validateParams, model.NSV3Client.Auth)
	if vErr != nil {
		return nil, &Error{
			Msg: "Failed to validate Nitrado token",
			Err: vErr,
			Ctx: ctx,
		}
	}

	if !validateResp.Payload.IsValid {
		return nil, &Error{
			Msg: "Invalid Nitrado token provided",
			Err: errors.New("invalid nitrado token"),
			Ctx: ctx,
		}
	}

	if validateResp.Payload.DuplicateToken != nil && *validateResp.Payload.DuplicateToken.ID != "" {
		return nil, &Error{
			Msg: "A token for this Nitrado account already exists",
			Err: errors.New("duplicate token"),
			Ctx: ctx,
		}
	}

	atResp, atErr := guildconfigservice.AddNitradoToken(ctx, model.GuildConfigClient, setupNitradoToken.GuildID, model.NitradoToken)
	if atErr != nil {
		return nil, &Error{
			Msg: "Failed to add new Nitrado token",
			Err: atErr.Err,
			Ctx: ctx,
		}
	}

	tokenName := atResp.Payload.NitradoToken.Token

	// GENERATE FIELDS
	fields := generateSetupAddTokenFields(tokenName)

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Setup Servers command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateSetupAddTokenFields(tokenName string) []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   "**New Nitrado Token has been added**",
		Value:  fmt.Sprintf("You can now run the `/setup servers` command to link your servers for this Nitrado account to the bot.\n\nToken Name: %s", tokenName),
		Inline: false,
	})

	return fields
}
