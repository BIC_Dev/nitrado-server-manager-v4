package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type ServerRemove struct {
	Config            *configs.Config
	GuildID           string
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
	ServerID          string
}

func (model *ServerRemove) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: fErr.Message,
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "Servers"); vErr != nil {
		return nil, &Error{
			Msg: vErr.Message,
			Err: vErr.Err,
			Ctx: ctx,
		}
	}

	var foundServer *gcscmodels.Server
	for _, server := range feed.Payload.Guild.Servers {
		if fmt.Sprint(server.NitradoID) == model.ServerID {
			foundServer = server
			break
		}
	}
	if foundServer == nil {
		return nil, &Error{
			Msg: fmt.Sprintf("Server %s is not one of your linked servers", model.ServerID),
			Err: errors.New("invalid server id"),
			Ctx: ctx,
		}
	}

	_, rnErr := guildconfigservice.DeleteServer(ctx, model.GuildConfigClient, foundServer)
	if rnErr != nil {
		return nil, &Error{
			Msg: rnErr.Message,
			Err: rnErr.Err,
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	fields := generateServerRemoveFields(model.ServerID)

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Server Remove command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateServerRemoveFields(serverID string) []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   fmt.Sprintf("**Removed server: %s**", serverID),
		Value:  "You should no longer see the server linked to the bot. To add it back, please run setup again.",
		Inline: false,
	})

	return fields
}
