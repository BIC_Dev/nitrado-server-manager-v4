package models

import (
	"context"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type InfoServers struct {
	Config            *configs.Config
	GuildID           string
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (model *InfoServers) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: fErr.Message,
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "Servers"); vErr != nil {
		return nil, &Error{
			Msg: vErr.Message,
			Err: vErr.Err,
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	fields := generateInfoServersFields(feed.Payload.Guild.Servers)

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Info Servers command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateInfoServersFields(servers []*gcscmodels.Server) []discord.Field {
	var fields []discord.Field

	for _, server := range servers {
		field := discord.Field{
			Name:   fmt.Sprintf("__%s__", server.Name),
			Value:  fmt.Sprintf("**Server ID:** %d\n", server.NitradoID),
			Inline: false,
		}

		if server.ServerType != nil {
			field.Value += fmt.Sprintf("**Server Type:** %s\n", server.ServerType.ID)
		}

		if server.BoostSetting != nil && server.BoostSetting.Code != "" {
			field.Value += fmt.Sprintf("**Boost Code:** %s\n", server.BoostSetting.Code)
		}

		// if server.NitradoToken != nil {
		// 	field.Value += fmt.Sprintf("**Nitrado Token ID:** %s\n", server.NitradoToken.Token)
		// }

		if server.ServerOutputChannels != nil {
			field.Value += "\n**__Output Channels__**\n"
			for _, oc := range server.ServerOutputChannels {
				field.Value += fmt.Sprintf("**%s:** <#%s>\n", oc.OutputChannelTypeID, oc.ChannelID)
			}
		}

		field.Value += "\n\u200B"

		fields = append(fields, field)
	}

	return fields
}
