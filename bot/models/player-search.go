package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient/players"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"
	"go.uber.org/zap"
)

var MAX_PLAYERS_FULL_FIELDS int = 20

type PlayerSearch struct {
	Config            *configs.Config
	GuildID           string
	Account           string
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (model *PlayerSearch) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: fErr.Message,
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "Servers"); vErr != nil {
		return nil, &Error{
			Msg: vErr.Message,
			Err: vErr.Err,
			Ctx: ctx,
		}
	}

	servers := ParseGuildConfigServersToNSV3Servers(feed.Payload.Guild.Servers, "")
	if len(servers) == 0 {
		return nil, &Error{
			Msg: "No servers linked to bot",
			Err: errors.New("no servers"),
			Ctx: ctx,
		}
	}

	// GET SEARCH RESULTS FROM API
	online := false
	params := players.NewPlayerSearchParamsWithTimeout(model.Config.NitradoServiceV3.Timeout * time.Second)
	params.SetContext(context.Background())
	params.SetBody(players.PlayerSearchBody{
		Name:    &model.Account,
		Online:  &online,
		Servers: servers,
	})

	resp, err := model.NSV3Client.Client.Players.PlayerSearch(params, model.NSV3Client.Auth)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to retrive players for Player Search",
			Err: err,
			Ctx: ctx,
		}
	}

	if resp.Payload == nil {
		return nil, &Error{
			Msg: "Failed to retrive players for Player Search",
			Err: errors.New("payload is nil"),
			Ctx: ctx,
		}
	}

	if resp.Payload.Players == nil || len(resp.Payload.Players) == 0 {
		return nil, &Error{
			Msg: fmt.Sprintf("No players found for: %s", model.Account),
			Err: errors.New("players is empty"),
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	var fields []discord.Field

	if len(resp.Payload.Players) <= MAX_PLAYERS_FULL_FIELDS {
		fields = generatePlayerSearchFields(resp.Payload.Players)
	} else {
		fields = generatePlayerSearchMinifiedFields(resp.Payload.Players)
	}

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Player Search command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generatePlayerSearchFields(p []*nsmodels.Player) []discord.Field {
	var fields []discord.Field

	for _, aPlayer := range p {
		status := "Offline"
		if aPlayer.Online == "true" {
			status = "Online"
		}

		fields = append(fields, discord.Field{
			Name:   fmt.Sprintf("**%s**", aPlayer.Name),
			Value:  fmt.Sprintf("Server ID: %s\nStatus: %s", aPlayer.LastServerID, status),
			Inline: false,
		})
	}

	return fields
}

func generatePlayerSearchMinifiedFields(p []*nsmodels.Player) []discord.Field {
	var fields []discord.Field

	count := 0
	var tempField discord.Field
	for _, aPlayer := range p {
		if count != 0 && count%5 == 0 {
			fields = append(fields, tempField)
			tempField = discord.Field{
				Name:   "\u200B",
				Inline: false,
			}
		}

		status := "Offline"
		if aPlayer.Online == "true" {
			status = "Online"
		}

		tempField.Value += fmt.Sprintf("\n%s : %s\n\u200B", aPlayer.Name, status)
	}

	if tempField.Value != "" {
		fields = append(fields, tempField)
	}

	return fields
}
