package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient/players"
	"go.uber.org/zap"
)

type PlayerBan struct {
	Config            *configs.Config
	GuildID           string
	Account           string
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
	ServerID          string
	Reason            string
}

func (model *PlayerBan) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: fErr.Message,
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "Servers"); vErr != nil {
		return nil, &Error{
			Msg: vErr.Message,
			Err: vErr.Err,
			Ctx: ctx,
		}
	}

	servers := ParseGuildConfigServersToNSV3Servers(feed.Payload.Guild.Servers, model.ServerID)
	if len(servers) == 0 {
		if model.ServerID == "" {
			return nil, &Error{
				Msg: "No servers linked to bot",
				Err: errors.New("no servers"),
				Ctx: ctx,
			}
		}

		return nil, &Error{
			Msg: fmt.Sprintf("Server %s is not one of your linked servers", model.ServerID),
			Err: errors.New("invalid server id"),
			Ctx: ctx,
		}
	}

	var successBans int
	var failedBans int
	for i := 0; i < len(servers); i++ {
		if i%5 == 0 {
			end := i + 5
			if end > len(servers) {
				end = len(servers)
			}

			params := players.NewPlayerBanParamsWithTimeout(model.Config.NitradoServiceV3.Timeout * time.Second)
			params.SetContext(context.Background())
			params.SetBody(players.PlayerBanBody{
				Name:    &model.Account,
				Servers: servers[i:end],
			})

			resp, err := model.NSV3Client.Client.Players.PlayerBan(params, model.NSV3Client.Auth)
			if err != nil {
				failedBans += end - i
				continue
			}

			successBans += len(resp.Payload.SuccessServerIds)
			failedBans += len(resp.Payload.FailedServers)
		}
	}

	// GENERATE FIELDS
	fields := generatePlayerBanFields(model.Account, model.Reason, successBans, failedBans)

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Player Ban command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generatePlayerBanFields(name string, reason string, success int, failures int) []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   fmt.Sprintf("**%s banned on %d servers**", name, success),
		Value:  fmt.Sprintf("Ban Reason: %s", reason),
		Inline: false,
	})

	if failures > 0 {
		fields = append(fields, discord.Field{
			Name:   fmt.Sprintf("**Failed to ban on %d servers**", failures),
			Value:  "If the user ban failed on all servers, please verify the account name and try again. If this problem persists, please open a support ticket in the BIC Development Discord.",
			Inline: false,
		})
	}

	return fields
}
