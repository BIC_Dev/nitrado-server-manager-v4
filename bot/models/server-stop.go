package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsclient/gameservers"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"
	"go.uber.org/zap"
)

type ServerStop struct {
	Config            *configs.Config
	GuildID           string
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
	ServerID          string
	User              *discordgo.User
	Message           string
}

func (model *ServerStop) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: fErr.Message,
			Err: fErr.Err,
			Ctx: ctx,
		}
	}

	if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "Servers"); vErr != nil {
		return nil, &Error{
			Msg: vErr.Message,
			Err: vErr.Err,
			Ctx: ctx,
		}
	}

	servers := ParseGuildConfigServersToNSV3Servers(feed.Payload.Guild.Servers, model.ServerID)
	if len(servers) == 0 {
		return nil, &Error{
			Msg: fmt.Sprintf("Server %s is not one of your linked servers", model.ServerID),
			Err: errors.New("invalid server id"),
			Ctx: ctx,
		}
	}

	// BAN PLAYER ACROSS SERVERS
	params := gameservers.NewGameserverStopParamsWithTimeout(model.Config.NitradoServiceV3.Timeout * time.Second)
	params.SetContext(context.Background())
	params.SetGameserverID(*servers[0].ID)
	params.SetBody(gameservers.GameserverStopBody{
		Message: &model.Message,
		Token:   servers[0].Token,
		User: &nsmodels.User{
			ID:   &model.User.ID,
			Name: &model.User.Username,
		},
	})

	_, err := model.NSV3Client.Client.Gameservers.GameserverStop(params, model.NSV3Client.Auth)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to stop server",
			Err: err,
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	fields := generateServerStopFields(servers[0])

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Server Stop command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateServerStopFields(server *nsmodels.Server) []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   fmt.Sprintf("**Stopped server: %s**", *server.ID),
		Value:  "A force stop has been requested for your server. Please give it 30 seconds to stop.",
		Inline: false,
	})

	return fields
}
