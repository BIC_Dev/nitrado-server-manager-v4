package models

import (
	"context"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/nitrado-service-v3-client/nsmodels"
)

type Error struct {
	Msg string
	Err error
	Ctx context.Context
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func (e *Error) Context() context.Context {
	return e.Ctx
}

func (e *Error) Message() string {
	return e.Msg
}

type Models interface {
	GetResponse(ctx context.Context) (*discordgo.InteractionResponse, *Error)
}

func ParseGuildConfigServersToNSV3Servers(servers []*gcscmodels.Server, identifier string) []*nsmodels.Server {
	var parsedServers []*nsmodels.Server
	for _, server := range servers {
		if fmt.Sprint(server.NitradoID) != identifier && identifier != "" {
			continue
		}

		serverID := fmt.Sprint(server.NitradoID)
		tokenID := fmt.Sprint(server.NitradoTokenID)

		tokenValue := ""
		if server.NitradoToken != nil {
			tokenValue = server.NitradoToken.TokenValue
		}

		token := nsmodels.Token{
			ID:    &tokenID,
			Value: &tokenValue,
		}
		parsedServers = append(parsedServers, &nsmodels.Server{
			ID:    &serverID,
			Token: &token,
		})
	}

	return parsedServers
}
