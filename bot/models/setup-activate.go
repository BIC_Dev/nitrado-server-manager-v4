package models

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type SetupActivate struct {
	Config            *configs.Config
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	Cache             *cache.Cache
	GuildConfigClient *guildconfigservice.GuildConfigService
	GuildID           string
	GuildName         string
	ActivationToken   string
}

func (model *SetupActivate) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var activationToken *cache.ActivationToken
	cacheKey := cache.GenerateKey(model.Config.CacheSettings.ActivationToken.Base, model.ActivationToken)
	cErr := model.Cache.GetStruct(ctx, cacheKey, &activationToken)
	if cErr != nil {
		return nil, &Error{
			Msg: "Unable to retrieve activation token",
			Err: cErr,
			Ctx: ctx,
		}
	}

	if activationToken == nil {
		return nil, &Error{
			Msg: "Activation token does not exist",
			Err: errors.New("invalid activation token"),
			Ctx: ctx,
		}
	}

	guildNeedsCreation := false
	guildNeedsService := true
	guildNeedsServiceActivated := false
	var existingGuildService *gcscmodels.GuildService

	// GET SERVERS FROM GUILD CONFIG SERVICE
	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		if _, ok := fErr.Err.(*guild_feeds.GetGuildFeedByIDNotFound); !ok {
			return nil, &Error{
				Msg: fErr.Message,
				Err: fErr.Err,
				Ctx: ctx,
			}
		}

		guildNeedsCreation = true
	} else {
		if vErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "GuildServices"); vErr == nil {
			for _, gs := range feed.Payload.Guild.GuildServices {
				if gs.Name == model.Config.Bot.GuildService {
					guildNeedsService = false

					if !gs.Enabled {
						guildNeedsServiceActivated = true
						existingGuildService = gs
					}
					break
				}
			}
		}
	}

	if !guildNeedsCreation && !guildNeedsService && !guildNeedsServiceActivated {
		return nil, &Error{
			Msg: "This bot is already activated",
			Err: errors.New("already activated"),
			Ctx: ctx,
		}
	}

	if guildNeedsCreation {
		_, gErr := guildconfigservice.CreateGuild(ctx, model.GuildConfigClient, model.GuildID, model.GuildName)
		if gErr != nil {
			return nil, &Error{
				Msg: "Failed to create guild",
				Err: gErr.Err,
				Ctx: ctx,
			}
		}
	}

	if guildNeedsService {
		_, gsErr := guildconfigservice.CreateGuildService(ctx, model.GuildConfigClient, model.GuildID, model.Config.Bot.GuildService)
		if gsErr != nil {
			return nil, &Error{
				Msg: "Failed to create guild service",
				Err: gsErr.Err,
				Ctx: ctx,
			}
		}
	}

	if guildNeedsServiceActivated {
		_, gsErr := guildconfigservice.ActivateGuildService(ctx, model.GuildConfigClient, existingGuildService, model.GuildName)
		if gsErr != nil {
			return nil, &Error{
				Msg: "Failed to activate existing guild service",
				Err: gsErr.Err,
				Ctx: ctx,
			}
		}
	}

	expErr := model.Cache.Expire(ctx, cacheKey)
	if expErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", expErr), zap.String("error_message", expErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
	}

	// GENERATE FIELDS
	fields := generateActivateFields(guildNeedsCreation, guildNeedsService, guildNeedsServiceActivated)

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Player Ban command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateActivateFields(guilldNeedsCreation bool, guildNeedsService bool, guildNeedsServiceActivated bool) []discord.Field {
	var fields []discord.Field

	activated := "Activated"
	if guildNeedsServiceActivated {
		activated = "Reactivated"
	}

	field := discord.Field{
		Name:   fmt.Sprintf("**Nitrado Server Manager V4 %s**", activated),
		Value:  "Actions:\n",
		Inline: false,
	}

	if guilldNeedsCreation {
		field.Value += "- Discord Server Linked\n"
	}

	if guildNeedsService || guildNeedsServiceActivated {
		field.Value += "- Nitrado Server Manager V4 Activated"
	}

	fields = append(fields, field)

	return fields
}
