package models

import (
	"context"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/nitrado_setups"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type SetupServers struct {
	Config            *configs.Config
	Command           *configs.Command
	CommandOption     *configs.CommandOption
	Cache             *cache.Cache
	GuildConfigClient *guildconfigservice.GuildConfigService
	GuildID           string
	SetupUser         *discordgo.User
}

func (model *SetupServers) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	feed, fErr := guildconfigservice.GetGuildFeed(ctx, model.GuildConfigClient, model.GuildID)
	if fErr != nil {
		return nil, &Error{
			Msg: "Failed to find linked Discord server",
			Err: fErr,
			Ctx: ctx,
		}
	}

	gvErr := guildconfigservice.ValidateGuildFeed(feed, model.Config.Bot.GuildService, "GuildServices")
	if gvErr != nil {
		return nil, &Error{
			Msg: "Bot is not activated for this Discord server",
			Err: gvErr,
			Ctx: ctx,
		}
	}

	nitradoSetupBody := gcscmodels.CreateNitradoSetupRequest{
		GuildID:          model.GuildID,
		GuildName:        feed.Payload.Guild.Name,
		ContactID:        model.SetupUser.ID,
		ContactName:      model.SetupUser.Username,
		GuildServiceName: model.Config.Bot.GuildService,
	}
	createNitradoSetupParams := nitrado_setups.NewCreateNitradoSetupParamsWithTimeout(60 * time.Second)
	createNitradoSetupParams.SetContext(context.Background())
	createNitradoSetupParams.SetGuild(model.GuildID)
	createNitradoSetupParams.SetBody(&nitradoSetupBody)
	nitradoSetupResponse, cnsErr := model.GuildConfigClient.Client.NitradoSetups.CreateNitradoSetup(createNitradoSetupParams, model.GuildConfigClient.Auth)
	if cnsErr != nil {
		return nil, &Error{
			Msg: "Unable to perform Nitrado setup",
			Err: cnsErr,
			Ctx: ctx,
		}
	}

	// GENERATE FIELDS
	fields := generateSetupServersFields(len(feed.Payload.Guild.Servers), len(nitradoSetupResponse.Payload.Servers)+len(feed.Payload.Guild.Servers), len(feed.Payload.Guild.NitradoTokens))

	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         model.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       fmt.Sprintf("**%s**", model.CommandOption.Title),
		Description: model.CommandOption.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       model.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    model.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Setup Servers command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}

func generateSetupServersFields(existingServers int, totalServers int, totalAccounts int) []discord.Field {
	var fields []discord.Field

	fields = append(fields, discord.Field{
		Name:   fmt.Sprintf("**Accounts Linked: %d**", totalAccounts),
		Value:  fmt.Sprintf("Servers Linked:\n- Existing Servers: %d\n- New Servers: %d", existingServers, totalServers-existingServers),
		Inline: false,
	})

	return fields
}
