package models

import (
	"context"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type Help struct {
	Config         *configs.Config
	SubCommandName string
}

func (help *Help) GetResponse(ctx context.Context) ([]*discordgo.MessageEmbed, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var command *configs.Command

	for _, aCommand := range help.Config.Commands {
		if aCommand.Name != help.SubCommandName {
			continue
		}

		command = &aCommand
		break
	}

	if command == nil {
		return nil, &Error{
			Msg: "Unable to find help command in config",
			Err: fmt.Errorf("command not found in config: help"),
			Ctx: ctx,
		}
	}

	fields := []discord.Field{}

	for _, option := range command.Options {
		if option.Type != 1 {
			continue
		}

		description := option.Description

		if len(option.Options) >= 1 {
			description += "\n\n\u200B\u200B\u200B\u200B__Parameters__"
		}

		for _, subCommandOption := range option.Options {
			commandRequiredText := "OPTIONAL"

			if subCommandOption.Required {
				commandRequiredText = "REQUIRED"
			}

			description += fmt.Sprintf("\n\u200B\u200B\u200B\u200B**%s** [%s]: %s", subCommandOption.Name, commandRequiredText, subCommandOption.Description)
		}

		fields = append(fields, discord.Field{
			Name:   fmt.Sprintf("**/help %s**", option.Name),
			Value:  description + "\n\u200B",
			Inline: false,
		})
	}

	embedResponse := discord.Response{
		URL:         help.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       "**" + command.Title + "**",
		Description: command.Description,
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       help.Config.Bot.OkColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    help.Config.Bot.OkThumbnail,
			Width:  100,
			Height: 100,
		},
		Fields: fields,
	}

	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return nil, &Error{
			Msg: "Failed to generate embed for Help command",
			Err: err,
			Ctx: ctx,
		}
	}

	return generatedEmbeds, nil
}
