package commands

import (
	"context"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type Error struct {
	Msg string
	Err error
	Ctx context.Context
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func (e *Error) Context() context.Context {
	return e.Ctx
}

func (e *Error) Message() string {
	return e.Msg
}

type Commands struct {
	Config *configs.Config
}

func (commands *Commands) GetApplicationCommands(ctx context.Context) ([]*discordgo.ApplicationCommand, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var applicationCommands []*discordgo.ApplicationCommand

	for _, command := range commands.Config.Commands {
		dmPermission := command.DMPermission
		var defaultMemberPermissions int64 = 0

		applicationCommand := &discordgo.ApplicationCommand{
			Name:                     command.Name,
			Description:              command.Description,
			Options:                  []*discordgo.ApplicationCommandOption{},
			DMPermission:             &dmPermission,
			DefaultMemberPermissions: &defaultMemberPermissions,
		}

		for _, option := range command.Options {
			// If the option is not enabled, we don't want to add it to the help information
			if !option.Enabled {
				continue
			}

			// We only want to add subcommands (type == 1) to the help command
			if option.Type != 1 {
				continue
			}

			options, err := ParseOptions(ctx, option.Options)
			if err != nil {
				eCtx := logging.AddValues(err.Ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Msg))
				logger := logging.Logger(eCtx)
				logger.Error("error_log")
				continue
			}

			channelTypes := []discordgo.ChannelType{}
			for _, cType := range option.ChannelTypes {
				channelTypes = append(channelTypes, discordgo.ChannelType(cType))
			}

			var choices []*discordgo.ApplicationCommandOptionChoice
			for _, choice := range option.Choices {
				choices = append(choices, &discordgo.ApplicationCommandOptionChoice{
					Name:  choice.Name,
					Value: choice.Value,
				})
			}

			applicationCommand.Options = append(applicationCommand.Options, &discordgo.ApplicationCommandOption{
				Name:         option.Name,
				Description:  option.Description,
				Required:     false,
				Type:         discordgo.ApplicationCommandOptionType(option.Type),
				Autocomplete: option.Autocomplete,
				ChannelTypes: channelTypes,
				Options:      options,
				Choices:      choices,
			})
		}

		applicationCommands = append(applicationCommands, applicationCommand)
	}

	return applicationCommands, nil
}

func ParseOptions(ctx context.Context, options []configs.CommandOption) ([]*discordgo.ApplicationCommandOption, *Error) {
	if len(options) == 0 {
		return nil, nil
	}

	var parsedOptions []*discordgo.ApplicationCommandOption

	for _, option := range options {
		subOptions, err := ParseOptions(ctx, option.Options)
		if err != nil {
			eCtx := logging.AddValues(err.Ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Msg))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")
			continue
		}

		channelTypes := []discordgo.ChannelType{}
		for _, cType := range option.ChannelTypes {
			channelTypes = append(channelTypes, discordgo.ChannelType(cType))
		}

		var choices []*discordgo.ApplicationCommandOptionChoice
		for _, choice := range option.Choices {
			choices = append(choices, &discordgo.ApplicationCommandOptionChoice{
				Name:  choice.Name,
				Value: choice.Value,
			})
		}

		parsedOptions = append(parsedOptions, &discordgo.ApplicationCommandOption{
			Name:         option.Name,
			Description:  option.Description,
			Required:     option.Required,
			Type:         discordgo.ApplicationCommandOptionType(option.Type),
			ChannelTypes: channelTypes,
			Options:      subOptions,
			Autocomplete: option.Autocomplete,
			Choices:      choices,
		})
	}

	return parsedOptions, nil
}
