package bot

import (
	"context"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/handlers"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

type Bot struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

type Error struct {
	Msg string
	Err error
	Ctx context.Context
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func (e *Error) Context() context.Context {
	return e.Ctx
}

func (e *Error) Message() string {
	return e.Msg
}

func (bot *Bot) StartBot(ctx context.Context) *Error {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	appCommands, rcErr := bot.RegisterCommands(ctx)
	if rcErr != nil {
		return rcErr
	} else {
		acCtx := logging.AddValues(ctx, zap.String("message", fmt.Sprintf("Successfully registered %d commands", len(appCommands))))
		logger := logging.Logger(acCtx)
		logger.Info("setup_log")
	}

	handler := handlers.Handlers{
		Config:            bot.Config,
		Environment:       bot.Environment,
		Session:           bot.Session,
		Cache:             bot.Cache,
		NSV3Client:        bot.NSV3Client,
		GuildConfigClient: bot.GuildConfigClient,
	}

	bot.Session.AddHandler(handler.InteractionHandler)

	return nil
}

func OpenDiscordSession(ctx context.Context, token string) (*discordgo.Session, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	dg, err := discordgo.New("Bot " + token)

	if err != nil {
		return nil, &Error{
			Msg: "Failed to create Discord client.",
			Err: err,
			Ctx: ctx,
		}
	}

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		return nil, &Error{
			Msg: "Failed to open Discord web socket.",
			Err: err,
			Ctx: ctx,
		}
	}

	return dg, nil
}

func (bot *Bot) RegisterCommands(ctx context.Context) ([]*discordgo.ApplicationCommand, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	cmds := commands.Commands{
		Config: bot.Config,
	}

	applicationCommands, err := cmds.GetApplicationCommands(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	var newCommands []*discordgo.ApplicationCommand

	if bot.Environment == "local" {
		for _, guild := range bot.Session.State.Guilds {
			guildNewCommands, err := bot.Session.ApplicationCommandBulkOverwrite(bot.Session.State.User.ID, guild.ID, applicationCommands)
			if err != nil {
				return nil, &Error{
					Msg: fmt.Sprintf("Failed to register commands with Discord guild: %s.", guild.ID),
					Err: err,
					Ctx: ctx,
				}
			} else {
				newCommands = append(newCommands, guildNewCommands...)
			}
		}
	} else {
		allGuildNewCommands, err := bot.Session.ApplicationCommandBulkOverwrite(bot.Session.State.User.ID, "", applicationCommands)
		if err != nil {
			return nil, &Error{
				Msg: "Failed to register commands with all Discord guilds.",
				Err: err,
				Ctx: ctx,
			}
		} else {
			newCommands = append(newCommands, allGuildNewCommands...)
		}
	}

	return newCommands, nil
}
