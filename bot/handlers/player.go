package handlers

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Handlers struct
type Player struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (player *Player) Handle(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	if len(ic.ApplicationCommandData().Options) == 0 {
		return nil, &Error{
			Msg: "Invalid interaction usage",
			Err: errors.New("interaction missing options"),
			Ctx: ctx,
		}
	}

	switch ic.ApplicationCommandData().Options[0].Name {
	case "ban":
		return player.Ban(ctx, ic)
	case "unban":
		return player.Unban(ctx, ic)
	case "search":
		return player.Search(ctx, ic)
	default:
		return nil, &Error{
			Msg: "Invalid interaction option",
			Err: fmt.Errorf("interaction option not supported: %s", ic.ApplicationCommandData().Options[0].Name),
			Ctx: ctx,
		}
	}
}

func (player *Player) Search(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.PlayerSearch{
		Config:            player.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        player.NSV3Client,
		GuildConfigClient: player.GuildConfigClient,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "account" {
			model.Account = subOpt.Value.(string)
		}
	}

	for _, aCommand := range player.Config.Commands {
		if aCommand.Name == "player" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "search" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (player *Player) Ban(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	model := models.PlayerBan{
		Config:            player.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        player.NSV3Client,
		GuildConfigClient: player.GuildConfigClient,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "account" {
			model.Account = subOpt.Value.(string)
		}

		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}

		if subOpt.Name == "reason" {
			model.Reason = subOpt.Value.(string)
		}
	}

	for _, aCommand := range player.Config.Commands {
		if aCommand.Name == "player" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "ban" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (player *Player) Unban(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	model := models.PlayerUnban{
		Config:            player.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        player.NSV3Client,
		GuildConfigClient: player.GuildConfigClient,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "account" {
			model.Account = subOpt.Value.(string)
		}

		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}

		if subOpt.Name == "reason" {
			model.Reason = subOpt.Value.(string)
		}
	}

	for _, aCommand := range player.Config.Commands {
		if aCommand.Name == "player" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "unban" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}
