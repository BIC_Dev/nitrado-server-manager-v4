package handlers

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/discord"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Handlers struct
type Handlers struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

type Error struct {
	Msg string
	Err error
	Ctx context.Context
}

type Handler interface {
	Handle(context.Context, *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error)
}

func (e *Error) Error() string {
	return e.Err.Error()
}

func (e *Error) Context() context.Context {
	return e.Ctx
}

func (e *Error) Message() string {
	return e.Msg
}

func (h *Handlers) InteractionHandler(s *discordgo.Session, ic *discordgo.InteractionCreate) {
	ctx := context.Background()

	userID := ""
	if ic.Member != nil {
		userID = ic.Member.User.ID
	} else if ic.User != nil {
		userID = ic.User.ID
	}

	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("env", h.Environment),
		zap.String("guild_id", ic.GuildID),
		zap.String("user_id", userID),
		zap.String("command_name", ic.ApplicationCommandData().Name),
		zap.Any("command_params", ic.ApplicationCommandData().Options),
	)

	var aHandler Handler

	if ic.Type == 2 {
		switch ic.ApplicationCommandData().Name {
		case "help":
			aHandler = &Help{
				Config:      h.Config,
				Environment: h.Environment,
				Session:     h.Session,
				Cache:       h.Cache,
			}
		case "player":
			aHandler = &Player{
				Config:            h.Config,
				Environment:       h.Environment,
				Session:           h.Session,
				Cache:             h.Cache,
				NSV3Client:        h.NSV3Client,
				GuildConfigClient: h.GuildConfigClient,
			}
		case "server":
			aHandler = &Server{
				Config:            h.Config,
				Environment:       h.Environment,
				Session:           h.Session,
				Cache:             h.Cache,
				NSV3Client:        h.NSV3Client,
				GuildConfigClient: h.GuildConfigClient,
			}
		case "setup":
			aHandler = &Setup{
				Config:            h.Config,
				Environment:       h.Environment,
				Session:           h.Session,
				Cache:             h.Cache,
				NSV3Client:        h.NSV3Client,
				GuildConfigClient: h.GuildConfigClient,
			}
		case "info":
			aHandler = &Info{
				Config:            h.Config,
				Environment:       h.Environment,
				Session:           h.Session,
				Cache:             h.Cache,
				NSV3Client:        h.NSV3Client,
				GuildConfigClient: h.GuildConfigClient,
			}
		default:
			h.ErrorOutput(ctx, ic, errors.New("command not implemented"), fmt.Sprintf("Not implemented: %s", ic.ApplicationCommandData().Name))
			return
		}
	}
	// if ic.Type == 3 {
	// 	switch ic.MessageComponentData().CustomID {
	// 	}
	// }

	var command *configs.Command
	var commandOption *configs.CommandOption
	for _, aCommand := range h.Config.Commands {
		if aCommand.Name == ic.ApplicationCommandData().Name {
			command = &aCommand
			for _, anOption := range aCommand.Options {
				if len(ic.ApplicationCommandData().Options) == 0 {
					break
				}

				if anOption.Name == ic.ApplicationCommandData().Options[0].Name {
					commandOption = &anOption
					break
				}
			}
			break
		}
	}

	if (commandOption != nil && commandOption.SendDeferredMessage) || command.SendDeferredMessage {
		var response *discordgo.InteractionResponse = &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Content: "Please wait while the bot processes your request.",
			},
		}

		discord.InteractionResponse(s, ic.Interaction, response)
	}

	ir, err := aHandler.Handle(ctx, ic)
	if err != nil {
		eCtx := logging.AddValues(err.Ctx, zap.NamedError("error", err.Err), zap.String("error_message", err.Msg))
		logger := logging.Logger(eCtx)
		logger.Error("error_log")

		h.ErrorOutput(ctx, ic, err, err.Msg)
		return
	}

	if (commandOption != nil && commandOption.SendDeferredMessage) || command.SendDeferredMessage {
		discord.InteractionEdit(s, ic.Interaction, &discordgo.WebhookEdit{
			Embeds: &ir,
		})
	} else {
		irErr := discord.InteractionResponse(s, ic.Interaction, &discordgo.InteractionResponse{
			Type: discordgo.InteractionResponseChannelMessageWithSource,
			Data: &discordgo.InteractionResponseData{
				Embeds: ir[0:1],
			},
		})
		if irErr != nil {
			eCtx := logging.AddValues(ctx, zap.NamedError("error", irErr), zap.String("error_message", "failed to respond to interaction"))
			logger := logging.Logger(eCtx)
			logger.Error("error_log")

			return
		}

		if len(ir) > 1 {
			for _, anEmbed := range ir[1:] {
				_, msgErr := discord.SendMessageEmbed(s, ic.ChannelID, []*discordgo.MessageEmbed{anEmbed})
				if msgErr != nil {
					eCtx := logging.AddValues(ctx, zap.NamedError("error", msgErr), zap.String("error_message", "failed to send follow-up embed"))
					logger := logging.Logger(eCtx)
					logger.Error("error_log")

					return
				}
			}
		}
	}
}

func parseSlashCommand(ic *discordgo.InteractionCreate) map[string]interface{} {
	var options map[string]interface{} = make(map[string]interface{})
	for _, option := range ic.ApplicationCommandData().Options {
		options[option.Name] = option.Value
	}

	return options
}

func getUser(ic *discordgo.InteractionCreate) *discordgo.User {
	if ic.Member != nil && ic.Member.User != nil {
		return ic.Member.User
	} else if ic.User != nil {
		return ic.User
	}

	return &discordgo.User{}
}

func (handler *Handlers) ErrorOutput(ctx context.Context, ic *discordgo.InteractionCreate, err error, message string) *Error {
	// CREATE RESPONSE STRUCT
	embedResponse := discord.Response{
		URL:         handler.Config.Bot.DocumentationURL,
		Type:        discordgo.EmbedTypeRich,
		Title:       "**ERROR**",
		Description: fmt.Sprintf("**Error Message:** %s\n**Error:** %s", message, err.Error()),
		Timestamp:   time.Now().Format(time.RFC3339),
		Color:       handler.Config.Bot.ErrorColor,
		Footer: discord.Footer{
			Text: "Executed",
		},
		Thumbnail: discord.Thumbnail{
			URL:    handler.Config.Bot.ErrorThumbnail,
			Width:  100,
			Height: 100,
		},
	}

	// GENERATE EMBEDS
	generatedEmbeds, err := embedResponse.GenerateEmbeds(ctx)
	if err != nil {
		return &Error{
			Msg: "Failed to generate embed for Setup Servers command",
			Err: err,
			Ctx: ctx,
		}
	}

	var response *discordgo.InteractionResponse = &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: &discordgo.InteractionResponseData{},
	}

	response.Data.Embeds = generatedEmbeds

	irErr := discord.InteractionResponse(handler.Session, ic.Interaction, response)
	if irErr != nil {
		return &Error{
			Msg: "Failed to respond with error",
			Err: irErr,
			Ctx: ctx,
		}
	}

	return nil
}
