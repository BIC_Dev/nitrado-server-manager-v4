package handlers

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Server struct
type Setup struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (setup *Setup) Handle(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if len(ic.ApplicationCommandData().Options) == 0 {
		return nil, &Error{
			Msg: "Invalid interaction usage",
			Err: errors.New("interaction missing options"),
			Ctx: ctx,
		}
	}

	switch ic.ApplicationCommandData().Options[0].Name {
	case "activate":
		return setup.Activate(ctx, ic)
	case "servers":
		return setup.Servers(ctx, ic)
	case "nitrado_token":
		return setup.NitradoToken(ctx, ic)
	case "add_token":
		return setup.AddToken(ctx, ic)
	default:
		return nil, &Error{
			Msg: "Invalid interaction option",
			Err: fmt.Errorf("interaction option not supported: %s", ic.ApplicationCommandData().Options[0].Name),
			Ctx: ctx,
		}
	}
}

func (setup *Setup) Activate(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This command can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.SetupActivate{
		Config:            setup.Config,
		GuildID:           ic.GuildID,
		GuildConfigClient: setup.GuildConfigClient,
		Cache:             setup.Cache,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "discord_name" {
			model.GuildName = subOpt.Value.(string)
		}

		if subOpt.Name == "token" {
			model.ActivationToken = subOpt.Value.(string)
		}
	}

	for _, aCommand := range setup.Config.Commands {
		if aCommand.Name == "setup" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "activate" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (setup *Setup) Servers(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This command can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.SetupServers{
		Config:            setup.Config,
		GuildID:           ic.GuildID,
		GuildConfigClient: setup.GuildConfigClient,
		Cache:             setup.Cache,
		SetupUser:         getUser(ic),
	}

	for _, aCommand := range setup.Config.Commands {
		if aCommand.Name == "setup" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "servers" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (setup *Setup) NitradoToken(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This command can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.SetupNitradoToken{
		Config:            setup.Config,
		GuildID:           ic.GuildID,
		GuildConfigClient: setup.GuildConfigClient,
		Cache:             setup.Cache,
		User:              getUser(ic),
		Session:           setup.Session,
	}

	for _, aCommand := range setup.Config.Commands {
		if aCommand.Name == "setup" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "nitrado_token" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (setup *Setup) AddToken(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.User == nil {
		return nil, &Error{
			Msg: "This command can only be run in a DM with the bot.",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.SetupAddToken{
		Config:            setup.Config,
		GuildConfigClient: setup.GuildConfigClient,
		Cache:             setup.Cache,
		User:              getUser(ic),
		NSV3Client:        setup.NSV3Client,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "token" {
			model.NitradoToken = subOpt.Value.(string)

			if model.NitradoToken == "" {
				return nil, &Error{
					Msg: "Nitrado token was not provided in command",
					Err: errors.New("invalid Nitrado token"),
					Ctx: ctx,
				}
			}
		}
	}

	for _, aCommand := range setup.Config.Commands {
		if aCommand.Name == "setup" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "add_token" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}
