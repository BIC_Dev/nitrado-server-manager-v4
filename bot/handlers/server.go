package handlers

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Server struct
type Server struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (server *Server) Handle(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This command can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	if len(ic.ApplicationCommandData().Options) == 0 {
		return nil, &Error{
			Msg: "Invalid interaction usage",
			Err: errors.New("interaction missing options"),
			Ctx: ctx,
		}
	}

	switch ic.ApplicationCommandData().Options[0].Name {
	case "stop":
		return server.Stop(ctx, ic)
	case "restart":
		return server.Restart(ctx, ic)
	case "rename":
		return server.Rename(ctx, ic)
	case "remove":
		return server.Remove(ctx, ic)
	default:
		return nil, &Error{
			Msg: "Invalid interaction option",
			Err: fmt.Errorf("interaction option not supported: %s", ic.ApplicationCommandData().Options[0].Name),
			Ctx: ctx,
		}
	}
}

func (server *Server) Stop(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.ServerStop{
		Config:            server.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        server.NSV3Client,
		GuildConfigClient: server.GuildConfigClient,
		User:              getUser(ic),
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}

		if subOpt.Name == "message" {
			model.Message = subOpt.Value.(string)
		}
	}

	for _, aCommand := range server.Config.Commands {
		if aCommand.Name == "server" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "stop" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (server *Server) Restart(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.ServerRestart{
		Config:            server.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        server.NSV3Client,
		GuildConfigClient: server.GuildConfigClient,
		User:              getUser(ic),
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}

		if subOpt.Name == "message" {
			model.Message = subOpt.Value.(string)
		}
	}

	for _, aCommand := range server.Config.Commands {
		if aCommand.Name == "server" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "restart" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (server *Server) Rename(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.ServerRename{
		Config:            server.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        server.NSV3Client,
		GuildConfigClient: server.GuildConfigClient,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}

		if subOpt.Name == "name" {
			model.Name = subOpt.Value.(string)
		}
	}

	for _, aCommand := range server.Config.Commands {
		if aCommand.Name == "server" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "rename" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}

func (server *Server) Remove(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.ServerRemove{
		Config:            server.Config,
		GuildID:           ic.GuildID,
		NSV3Client:        server.NSV3Client,
		GuildConfigClient: server.GuildConfigClient,
	}

	for _, subOpt := range ic.ApplicationCommandData().Options[0].Options {
		if subOpt.Name == "server_id" {
			model.ServerID = subOpt.Value.(string)
		}
	}

	for _, aCommand := range server.Config.Commands {
		if aCommand.Name == "server" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "remove" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}
