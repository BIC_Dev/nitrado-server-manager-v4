package handlers

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/guildconfigservice"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/services/nsv3"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Info struct
type Info struct {
	Config            *configs.Config
	Environment       string
	Session           *discordgo.Session
	Cache             *cache.Cache
	NSV3Client        *nsv3.NSV3ClientWrapper
	GuildConfigClient *guildconfigservice.GuildConfigService
}

func (info *Info) Handle(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if ic.Member == nil {
		return nil, &Error{
			Msg: "This can only be run in a Discord server",
			Err: errors.New("invalid command execution"),
			Ctx: ctx,
		}
	}

	if len(ic.ApplicationCommandData().Options) == 0 {
		return nil, &Error{
			Msg: "Invalid interaction usage",
			Err: errors.New("interaction missing options"),
			Ctx: ctx,
		}
	}

	switch ic.ApplicationCommandData().Options[0].Name {
	case "servers":
		return info.Servers(ctx, ic)
	default:
		return nil, &Error{
			Msg: "Invalid interaction option",
			Err: fmt.Errorf("interaction option not supported: %s", ic.ApplicationCommandData().Options[0].Name),
			Ctx: ctx,
		}
	}
}

func (info *Info) Servers(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	options := ic.ApplicationCommandData().Options

	optionMap := make(map[string]*discordgo.ApplicationCommandInteractionDataOption, len(options))
	for _, opt := range options {
		optionMap[opt.Name] = opt
	}

	model := models.InfoServers{
		Config:            info.Config,
		GuildID:           ic.GuildID,
		GuildConfigClient: info.GuildConfigClient,
	}

	for _, aCommand := range info.Config.Commands {
		if aCommand.Name == "info" {
			model.Command = &aCommand
			for _, anOption := range aCommand.Options {
				if anOption.Name == "servers" {
					model.CommandOption = &anOption
					break
				}
			}
			break
		}
	}

	response, err := model.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}
