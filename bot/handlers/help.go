package handlers

import (
	"context"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/bot/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/configs"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-server-manager-v4/utils/logging"
	"go.uber.org/zap"
)

// Handlers struct
type Help struct {
	Config      *configs.Config
	Environment string
	Session     *discordgo.Session
	Cache       *cache.Cache
}

func (help *Help) Handle(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	return help.HelpCommand(ctx, ic)
}

func (help *Help) HelpCommand(ctx context.Context, ic *discordgo.InteractionCreate) ([]*discordgo.MessageEmbed, *Error) {
	logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	helpModel := models.Help{
		Config:         help.Config,
		SubCommandName: ic.ApplicationCommandData().Options[0].Name,
	}

	if helpModel.SubCommandName == "all" {
		helpModel.SubCommandName = "help"
	}

	response, err := helpModel.GetResponse(ctx)
	if err != nil {
		return nil, &Error{
			Msg: err.Msg,
			Err: err.Err,
			Ctx: err.Ctx,
		}
	}

	return response, nil
}
