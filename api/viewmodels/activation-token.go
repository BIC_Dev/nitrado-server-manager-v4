package viewmodels

// ActivationToken struct
type ActivationToken struct {
	Token string `json:"token"`
}

// CreateActivationTokenResponse struct
type CreateActivationTokenResponse struct {
	Message    string          `json:"message"`
	SetupToken ActivationToken `json:"setup_token"`
}
